//
//  ViewController.h
//  select_tableview
//
//  Created by lazybios on 5/27/15.
//  Copyright (c) 2015 lazybios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (nonatomic, strong) IBOutlet UITableView *table;
@end

