//
//  EditViewController.h
//  select_tableview
//
//  Created by lazybios on 5/28/15.
//  Copyright (c) 2015 lazybios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *nameField;
@property (strong, nonatomic) IBOutlet UITextField *detailField;

@property (strong, nonatomic) NSIndexPath* editingIndexPath;

- (IBAction)clicked:(id)sender;
- (IBAction)finshed:(id)sender;

@end
