//
//  EditViewController.m
//  select_tableview
//
//  Created by lazybios on 5/28/15.
//  Copyright (c) 2015 lazybios. All rights reserved.
//

#import "EditViewController.h"
#import "ViewController.h"
#import "AppDelegate.h"

@interface EditViewController ()

@end

@implementation EditViewController
AppDelegate* appDelegate;
NSUInteger rowNo;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    appDelegate = [UIApplication sharedApplication].delegate;
    rowNo =self.editingIndexPath.row;
    self.nameField.text = [appDelegate.books
                           objectAtIndex:rowNo];
    self.detailField.text = [appDelegate.details
                             objectAtIndex:rowNo];
}

-(IBAction)clicked:(id)sender
{
    [appDelegate.books replaceObjectAtIndex:rowNo withObject:self.nameField.text];
    [appDelegate.details replaceObjectAtIndex:rowNo withObject:self.detailField.text];
    
    EditViewController* listController = [self.storyboard instantiateViewControllerWithIdentifier:@"list"];
    
    appDelegate.window.rootViewController = listController;
}

- (IBAction)finshed:(id)sender{
    [sender resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
