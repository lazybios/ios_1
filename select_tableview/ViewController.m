//
//  ViewController.m
//  select_tableview
//
//  Created by lazybios on 5/27/15.
//  Copyright (c) 2015 lazybios. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "EditViewController.h"

@interface ViewController ()

@end

@implementation ViewController
AppDelegate * appDelegate;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.table.dataSource = self;
    self.table.delegate = self;
    appDelegate = [UIApplication sharedApplication].delegate;
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.table reloadData];
}

- (UITableViewCell *) tableView: (UITableView * ) tableView
          cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString* cellId = @"cellId";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleSubtitle
                reuseIdentifier:cellId];
    }
    
    NSUInteger rowNo = indexPath.row;
    
    cell.textLabel.text = [appDelegate.books objectAtIndex:rowNo];
    cell.imageView.image = [UIImage imageNamed:@"ic_gray.png"];
    cell.imageView.highlightedImage = [UIImage imageNamed:@"ic_highlight.png"];
    cell.detailTextLabel.text = [appDelegate.details objectAtIndex:rowNo];
    
    return cell;
}

- (NSInteger) tableView: (UITableView *) tableView
  numberOfRowsInSection:(NSInteger)section{
    
    return appDelegate.books.count;
}

- (void)tableView:(UITableView *) tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    EditViewController* detailController = [self.storyboard
                                            instantiateViewControllerWithIdentifier:@"detail"];
    detailController.editingIndexPath =indexPath;
    appDelegate.window.rootViewController = detailController;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
