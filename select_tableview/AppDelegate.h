//
//  AppDelegate.h
//  select_tableview
//
//  Created by lazybios on 5/27/15.
//  Copyright (c) 2015 lazybios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;



@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;


@property (strong, nonatomic) NSMutableArray* books;
@property (strong, nonatomic) NSMutableArray* details;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

